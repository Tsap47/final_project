#pragma once
#include <iostream>
#include <SFML/Graphics.hpp>

namespace mt
{
	class  Cheese
	{
	public:
		Cheese(int x, int y, float r);//��� �������� ���������� � ������
		~Cheese();

		bool Setup();

		sf::Sprite* Get();
		int GetX();
		int GetY();
		float GetR();

	private:
		int m_x, m_y;
		float m_r;

		sf::Texture m_texture;
		sf::Sprite* m_cheese = nullptr;
	};
}

