﻿// Подключение графической библиотеки
#include <SFML/Graphics.hpp>
#include <thread>
#include <chrono>
#include <cheese.hpp>
#include <iostream>
#include <mouse.hpp>

#pragma comment(linker, "/SUBSYSTEM:windows /ENTRY:mainCRTStartup")



using namespace std::chrono_literals;

int main()
{

    float t = 0;
    float sx = 550;
    float sy = 430;
   
    // Создание окна 
    sf::RenderWindow window(sf::VideoMode(800, 500), L" Мышь ищит сыр ");

    // Загрузка заднего фона
    sf::Texture texture;
    if (!texture.loadFromFile("img/back.jpg"))
    {
        std::cout << "ERROR when loading back.jpg" << std::endl;
        return false;
    }
    sf::Sprite back;
    back.setTexture(texture);

    
    // icon
    sf::Image icon;
    if (!icon.loadFromFile("img/logo.png"))
    {
        return -1;
    }
    window.setIcon(48, 48, icon.getPixelsPtr());
    


    // Генерация объектов
    std::vector<mt::Cheese*> earths;
    earths.push_back(new mt::Cheese(320, 200, 200));
    
  

    // Подгрузка картинок и завершение программы, если они не загрузились
    for (const auto& earth : earths)
        if (!earth->Setup())
            return -1;

    mt::Mouse* mouse = nullptr;

    // Цикл работает до тех пор, пока окно открыто
    while (window.isOpen())
    {
        // Переменная для хранения события
        sf::Event event;
        // Цикл по всем событиям
        while (window.pollEvent(event))
        {
            // Обработка событий
            // Если нажат крестик, то
            if (event.type == sf::Event::Closed)
                // окно закрывается
                window.close();
        }



        
        if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
        {

            // Дребезг контактов
            
            sf::Vector2i mp = sf::Mouse::getPosition(window);

            

            float ay = mp.y - sy;
            float ax = mp.x - sx;
            float angle = acos(ax / sqrt(ax * ax + ay * ay));
            //float angle = acos(ax / sqrt(ax * ax + ay * ay));

            if (mouse != nullptr)
                delete mouse;

            mouse = new mt::Mouse(sx, sy, 210, angle);


            if (!mouse->Setup())
            {
                delete mouse;
                window.close();
                return -1;
            }

            t = 0;
        } 
        

        // Мышь идет
        
        if (mouse != nullptr) //нулевой указатель
        {
            mouse->Move(t);
            
        }

        // Вывод на экрна
        window.clear();

        // Вывод фона
        window.draw(back);


        // Вывод сыра 
        for (const auto& earth : earths)
            window.draw(*earth->Get());

 
        
        // Вывод мыши

        if(mouse != nullptr)
            window.draw(*mouse->Get());

        // Отобразить на окне все, что есть в буфере
        window.display();

        //кадров в секунду
        std::this_thread::sleep_for(40ms);
        t += 0.04;
    }

    if (mouse != nullptr)
        delete mouse;

    return 0;
}
