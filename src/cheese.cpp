#include <cheese.hpp>

namespace mt//������������ ����
{
	Cheese::Cheese(int x, int y, float r)
	{
		m_x = x;
		m_y = y;
		m_r = r;
	}

	bool Cheese::Setup()
	{

		if (!m_texture.loadFromFile("img/cheese.png"))
		{
			std::cout << "ERROR" << std::endl;
			return false;
		}


		m_cheese = new sf::Sprite();
		m_cheese->setTexture(m_texture);
		m_cheese->setOrigin(m_r, m_r);
		m_cheese->setPosition(m_x, m_y);//������������� ������ � ������������ �����
		m_cheese->setScale(0.3, 0.3);

		return true;
	}

	Cheese::~Cheese()
	{
		if (m_cheese != nullptr)
			delete m_cheese;
	}

	sf::Sprite* Cheese::Get() { return m_cheese; }//����� get ����� �������� ���������� ������


	int Cheese::GetX() { return m_x; }
	int Cheese::GetY() { return m_y; }
	float Cheese::GetR() { return m_r; }
}
