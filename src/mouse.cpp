#include <mouse.hpp>
#include <iostream>

namespace mt
{
	
	Mouse::Mouse(int x0, int y0, float r, float angle)
	{
		m_x0 = x0;
		m_y0 = y0;
		m_r = r;
		m_angle = (2 * acos(-1) - angle);
		
	}

	bool Mouse::Setup()//������� ����
	{
		if (!m_texture.loadFromFile("img/mouse1.png"))
		{
			std::cout << "ERROR when loading mouse1.png" << std::endl;
			return false;
		}

		m_mouse = new sf::Sprite();
		m_mouse->setTexture(m_texture);
		m_mouse->setOrigin(m_r, m_r);
		m_mouse->setPosition(m_x, m_y);

		return true;
	}

	Mouse::~Mouse()
	{
		if (m_mouse != nullptr)
			delete m_mouse;
	}

	
	void Mouse::Move(float t)
	{


		m_x = m_x0 + m_r * cos(m_angle * t / 8);
		m_y = m_y0 + m_r * sin(m_angle * t/8);
		m_mouse->setPosition(m_x, m_y);
	}
	
	sf::Sprite* Mouse::Get() { return m_mouse; }//����� get ����� �������� ���������� ������

	int Mouse::GetX() { return m_x; }
	int Mouse::GetY() { return m_y; }
	float Mouse::GetR() { return m_r; }
}